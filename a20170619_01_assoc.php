<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="js/jquery-3.2.1.js"></script>
</head>
<body>
<pre>
<?php

$ar1 = array(
        'name' => 'flora',
        'age' => 23,
);
$ar2 = [
    'name' => 'flora',
    'age' => 23,
];

$br = $ar1;
$ar1['name'] = 'mary';


print_r($ar1);
print_r($ar2);
print_r($br);





?>
</pre>
</body>
</html>