<?php
require(__DIR__ . '/__connect_db.php');




$per_page = 5;

$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;

$result = array(
    'success' => false,
    'error_no' => '',
    'error' => '',
    'total_rows' => 0,
    'total_pages' => 0,
    'per_page' => $per_page,
    'page' => $page,
    'data' => '',
);

$t_sql = "SELECT COUNT(1) FROM `address_book`";
$t_rs = $mysqli->query($t_sql);
$t_row = $t_rs->fetch_row();

$result['total_rows'] = $t_row[0]; //總筆數
$result['total_pages'] = ceil($t_row[0]/$per_page);



$sql = sprintf("SELECT * FROM `address_book` 
            ORDER BY `id` DESC
            LIMIT %s, %s",
    ($page-1)*$per_page,
    $per_page
);


$rs = $mysqli->query($sql);
$result['data'] = $rs->fetch_all(MYSQLI_ASSOC);
$result['success'] = true;
unset($result['error']);
unset($result['error_no']);
echo json_encode($result, JSON_UNESCAPED_UNICODE);
