<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

$a = 5;

$b = $a or 10; // ($b = $a) or 10;
$c = $a || 10;

echo $b;
echo "<br>";
echo $c;
echo "<br>";
// -----------
$a = 0;

$b = $a or 10; // ($b = $a) or 10;
$c = $a || 10;

echo $b;
echo "<br>";
echo $c;
echo "<br>";



?>
</body>
</html>