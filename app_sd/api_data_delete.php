<?php
include __DIR__. '/__connect_db.php';

$result = array(
    'success' => false,
    'error_no' => '',
    'error' => '',
    'affected_rows' => 0,
    'id' => 0,
);

if(isset($_GET['id'])){
    $id = intval($_GET['id']);


    $sql = "DELETE FROM `address_book` WHERE `id`=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('i', $id);
    $stmt->execute();


    $result['id'] = $id;
    $result['affected_rows'] = $stmt->affected_rows;
    if($stmt->affected_rows==1){
        $result['success'] = true;
    }

    $stmt->close();
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);

