<?php
//include __DIR__. '/__connect_db.php';
require(__DIR__ . '/__connect_db.php');

$page_name = 'data_list';
$page_title = '資料列表';

$per_page = 5;
// $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;



$t_sql = "SELECT COUNT(1) FROM `address_book`";
$t_rs = $mysqli->query($t_sql);
$t_row = $t_rs->fetch_row();
$total_rows = $t_row[0]; //總筆數

$total_pages = ceil($total_rows/$per_page);

//exit;
//die('hello');

$sql = sprintf("SELECT * FROM `address_book` 
            ORDER BY `id` DESC
            LIMIT %s, %s",
    ($page-1)*$per_page,
    $per_page
);


$rs = $mysqli->query($sql);
?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">

    <?php include __DIR__. '/__navbar.php'; ?>

    <div class="row">
    <div class="col-sm-12">
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li class="<?= $page<=1 ? 'disabled' : '' ?>">
                    <a <?= $page<=1 ? '' : 'href="?page='. ($page-1). '"' ?> aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <?php for($i=1; $i<=$total_pages; $i++): ?>
                <li class="<?= $i==$page ? 'active' : '' ?>">
                    <a href="?page=<?= $i ?>"><?= $i ?></a>
                </li>
                <?php endfor; ?>
                <li class="<?= $page>=$total_pages ? 'disabled' : '' ?>">
                    <a href="<?= $page>=$total_pages ? '' : '?page='. ($page+1) ?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    </div>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>del</th>
        <th>#</th>
        <th>Name</th>
        <th>Mobile</th>
        <th>Email</th>
        <th>Address</th>
        <th>Birthday</th>
        <th>edit</th>
    </tr>
    </thead>
    <tbody>
    <?php while ($row = $rs->fetch_assoc()): ?>
        <tr>
            <td>
                <a href="javascript:delete_it(<?= $row['id'] ?>)">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </a>
            </td>
            <td><?= $row['id'] ?></td>
            <td><?= $row['name'] ?></td>
            <td><?= strip_tags($row['mobile']) ?></td>
            <td><?= $row['email'] ?></td>
            <td><?= $row['address'] ?></td>
            <td><?= $row['birthday'] ?></td>

            <td>
                <a href="data_edit.php?id=<?= $row['id'] ?>">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </a>
            </td>
        </tr>
    <?php endwhile; ?>

    </tbody>
</table>

</div>

    <script>
        function delete_it(id){
            if(confirm("您確定要刪除編號為 "+ id +" 的資料嗎？")){
                location.href = "data_delete.php?id="+id;
            }
        }
    </script>

<?php include __DIR__. '/__html_foot.php'; ?>