<?php
require __DIR__ . '/__connect_db.php';

$page_name = 'data_edit';
$page_title = '修改資料';

$id = isset($_GET['id']) ? intval($_GET['id']) : 1;


if( isset($_POST['name']) and !empty($_POST['name'])){

    $sql = "UPDATE `address_book` SET 
`name`=?,
`mobile`=?,
`email`=?,
`address`=?,
`birthday`=? 
WHERE `id`=?";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param("sssssi",
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['address'],
        $_POST['birthday'],
        $id
        );

    $stmt->execute();
    $a_rows = $stmt->affected_rows;
    //$stmt->affected_rows //影響的列數，修改的列數
}


$sql = "SELECT * FROM `address_book` WHERE id=$id";
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();

?>
<?php include __DIR__ . '/__html_head.php'; ?>
    <div class="container">

        <?php include __DIR__ . '/__navbar.php'; ?>

        <?php
        /*
        if(isset($a_rows)){
            echo $a_rows;
        }
        */
        ?>

        <?php if(isset($a_rows) and $a_rows==1):?>
        <div class="col-sm-12">
            <div class="alert alert-success" role="alert">
                修改資料完成
            </div>
        </div>
        <?php endif; ?>

        <div class="col-sm-6">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">修改資料</h3>
                </div>
                <div class="panel-body">

                    <form method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text"
                                   class="form-control"
                                   id="name"
                                   name="name"
                                   value="<?= $row['name'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile</label>
                            <input type="text"
                                   class="form-control"
                                   id="mobile"
                                   name="mobile"
                                   value="<?= $row['mobile'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text"
                                   class="form-control"
                                   id="email"
                                   name="email"
                                   value="<?= $row['email'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text"
                                   class="form-control"
                                   id="address"
                                   name="address"
                                   value="<?= $row['address'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="birthday">Birthday</label>
                            <input type="text"
                                   class="form-control"
                                   id="birthday"
                                   name="birthday"
                                   value="<?= $row['birthday'] ?>">
                        </div>
                        <button type="submit" class="btn btn-default">修改</button>
                    </form>
                </div>
            </div>
        </div>



    </div>
<?php include __DIR__ . '/__html_foot.php'; ?>