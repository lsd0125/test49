<?php
require(__DIR__ . '/__connect_db.php');

$result = array(
    'success' => false,
    'error_no' => '1001',
    'error' => '沒有傳指定 id !',
    'id' => 0,
    'post_data' => '',
    'affected_rows' => 0,
);

if(isset($_POST['id'])){
    unset($result['error']);
    unset($result['error_no']);

    $id = intval($_POST['id']);



    $sql = "UPDATE `address_book` SET 
        `name`=?,
        `mobile`=?,
        `email`=?,
        `address`=?,
        `birthday`=? 
    WHERE  `id`=$id ";

    // (1)
    $stmt = $mysqli->prepare($sql);

    // (2)
    $stmt->bind_param("sssss",
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['address'],
        $_POST['birthday']
        );

    // (3)
    $stmt->execute();

    $result['id'] = $id;
    $result['post_data'] = $_POST;
    $result['affected_rows'] = $stmt->affected_rows;
    $result['success'] = true;

    $stmt->close();
}
$mysqli->close();

echo json_encode($result, JSON_UNESCAPED_UNICODE);
