<?php
require __DIR__ . '/__connect_db.php';

$page_name = 'data_insert';
$page_title = '新增資料';

if( isset($_POST['name']) and !empty($_POST['name'])){

    $sql = "INSERT INTO `address_book`(
        `name`, 
        `mobile`, 
        `email`, 
        `address`, 
        `birthday`
        ) VALUES (?,?,?,?,?)";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param("sssss",
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['address'],
        $_POST['birthday']
        );

    $stmt->execute();
    $a_rows = $stmt->affected_rows;
    //$stmt->affected_rows //影響的列數，新增的列數
}


?>
<?php include __DIR__ . '/__html_head.php'; ?>
    <div class="container">

        <?php include __DIR__ . '/__navbar.php'; ?>

        <?php
        /*
        if(isset($a_rows)){
            echo $a_rows;
        }
        */
        ?>

        <?php if(isset($a_rows) and $a_rows==1):?>
        <div class="col-sm-12">
            <div class="alert alert-success" role="alert">
                新增資料完成
            </div>
        </div>
        <?php endif; ?>

        <div class="col-sm-6">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">新增資料</h3>
                </div>
                <div class="panel-body">

                    <form method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text"
                                   class="form-control"
                                   id="name"
                                   name="name"
                                   placeholder="姓名">
                        </div>
                        <div class="form-group">
                            <label for="mobile">Mobile</label>
                            <input type="text"
                                   class="form-control"
                                   id="mobile"
                                   name="mobile">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text"
                                   class="form-control"
                                   id="email"
                                   name="email">
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text"
                                   class="form-control"
                                   id="address"
                                   name="address">
                        </div>
                        <div class="form-group">
                            <label for="birthday">Birthday</label>
                            <input type="text"
                                   class="form-control"
                                   id="birthday"
                                   name="birthday"
                            >
                        </div>
                        <button type="submit" class="btn btn-default">新增</button>
                    </form>
                </div>
            </div>
        </div>



    </div>
<?php include __DIR__ . '/__html_foot.php'; ?>