<?php
require __DIR__ . '/__connect_db.php';

$result = array(
    'success' => false,
    'error_no' => '400',
    'error' => 'no post data',
    'affected_rows' => 0,
    'post' => '',
);

if( isset($_POST['name']) and !empty($_POST['name'])){

    $sql = "INSERT INTO `address_book`(
        `name`, 
        `mobile`, 
        `email`, 
        `address`, 
        `birthday`
        ) VALUES (?,?,?,?,?)";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param("sssss",
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['address'],
        $_POST['birthday']
        );

    $stmt->execute();
    $a_rows = $stmt->affected_rows;
    //$stmt->affected_rows //影響的列數，新增的列數

    $result['affected_rows'] = $a_rows;

    $result['post'] = $_POST; // 回傳資料做確認

    if($a_rows==1){
        $result['success'] = true;
        unset($result['error']);
        unset($result['error_no']);
    }
}


echo json_encode($result);
