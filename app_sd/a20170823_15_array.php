<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php
$ar = array(2, 3, 4);
$ar2 = [2, 3, 4, true];

$ar[] = 'abc'; // array_push()

print_r($ar);

$ar3 = array(
    'name' => 'shin',
    'age' => 30,
    'gender' => 'male',
);

$ar4 = [
    'name' => 'shin',
    'age' => 30,
    'gender' => 'male',
];

var_dump($ar4);
?>



</body>
</html>