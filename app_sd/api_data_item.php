<?php
require(__DIR__ . '/__connect_db.php');

$result = array(
    'success' => false,
    'error_no' => '1001',
    'error' => '沒有傳指定 id !',
    'id' => 0,
    'row' => '',
);

if(isset($_GET['id'])){
    unset($result['error']);
    unset($result['error_no']);

    $id = intval($_GET['id']);
    $sql = "SELECT * FROM `address_book` WHERE `id`=$id";
    $rs = $mysqli->query($sql);

    $result['id'] = $id;
    if($row = $rs->fetch_assoc()){
        $result['row'] = $row;
    }

    $result['success'] = true;

}

echo json_encode($result, JSON_UNESCAPED_UNICODE);
