
<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
date_default_timezone_set( 'Asia/Taipei' );

echo time();
echo '<br>';
echo date("Y-m-d H:i:s");
echo '<br>';
echo date("Y-m-d H:i:s", time()+15*24*60*60);

?>
</body>
</html>