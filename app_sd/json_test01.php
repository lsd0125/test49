<?php

$ar = array(
    'name' => '林小明',
    'age' => 32,
    'data' => "///",
    'aaa' => array(
        'a' => 555,
        'hi' => 'hhh'
    )
);

$str = json_encode($ar, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);


$br = json_decode($str);


var_dump($br);
echo "\n\n\n";
echo $br->name;

echo "\n\n\n";

$cr = json_decode($str, true);
var_dump($cr);
echo "\n\n\n";
echo $cr['name'];