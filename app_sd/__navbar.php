<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
            <a href="#" class="navbar-brand">Brand</a></div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li <?= $page_name=='data_list' ? 'class="active"' : '' ?>>
                    <a href="data_list.php">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                        資料列表
                    </a>
                </li>
                <li <?= $page_name=='data_insert' ? 'class="active"' : '' ?>>
                    <a href="data_insert.php">新增資料</a>
                </li>
<?php   ?>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
            </ul>
        </div>
    </div>
</nav>