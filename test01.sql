WKR0obZgyhPHfOuD
UPDATE `address_book` SET `name` = '史大巴' WHERE `sid` = 2;

INSERT INTO address_book (
  `sid`,
  `name`,
  `mobile`,
  `email`,
  `address`,
  `birthday`
) VALUES (
  NULL,
  '李小明',
  '0918-123456',
  'udsyfsu@dsfs.com',
  '台北市',
   '1991-5-5'
);


CREATE TABLE `mytest`.`address_book` (
    `sid` INT NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(255) NOT NULL ,
    `mobile` VARCHAR(255) NULL ,
    `email` VARCHAR(255) NULL ,
    `address` VARCHAR(255) NULL ,
    `birthday` DATE NULL ,
    PRIMARY KEY (`sid`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;