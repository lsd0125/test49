<?php
require __DIR__ . '/__connect_db.php';
$pname = 'data_insert';

$result = false;
if( isset($_POST['name']) ){

    $sql = sprintf("INSERT INTO `address_book`(
        `sid`, 
        `name`,
        `mobile`, 
        `email`,
        `address`,
        `birthday`
          ) VALUES (
          NULL,
          '%s', '%s', '%s', '%s', '%s'
          )",
            $mysqli->escape_string( $_POST['name'] ),
            $mysqli->escape_string( $_POST['mobile']),
            $mysqli->escape_string( $_POST['email']),
            $mysqli->escape_string( $_POST['address']),
            $mysqli->escape_string( $_POST['birthday'])
        );

    //echo $sql;
    $result = $mysqli->query($sql);


}


?>
<?php include __DIR__ . '/__page_head.php' ?>
    <style>
        .red {
            color: red;
        }
    </style>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>

        <?php if($result): ?>
        <div class="col-md-12" id="myinfo">
            <div class="alert alert-success" role="alert">
                資料新增完成
            </div>
        </div>
            <script>
                setTimeout(function(){
                    $('#myinfo').slideUp();
                }, 3000);
            </script>
        <?php endif ?>
        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">新增資料</h3></div>
                    <div class="panel-body">

                        <form method="post" onsubmit="return checkForm();">
                            <div class="form-group">
                                <label for="name">姓名</label>  <span class="red">請填寫姓名</span>
                                <input type="text" class="form-control" id="name" name="name" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="mobile">手機</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="email">電郵</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="address">地址</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="birthday">生日</label>
                                <input type="text" class="form-control" id="birthday" name="birthday" placeholder="">
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <script>
        function checkForm(){

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if($('#name').val().length < 3 ){
                alert('請填寫正確的姓名');
                return false;
            }

            if($('#mobile').val().length < 3 ){
                alert('請填寫手機號碼');
                return false;
            }

            var email = $('#email').val();

            if(! pattern.test(email)){
                alert('請填寫正確的 email');
                return false;
            }


            return true;
        }




    </script>
<?php include __DIR__ . '/__page_foot.php' ?>