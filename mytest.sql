-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2017-09-26 05:37:09
-- 伺服器版本: 10.1.26-MariaDB
-- PHP 版本： 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `mytest`
--
CREATE DATABASE IF NOT EXISTS `mytest` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mytest`;

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `address_book`
--

INSERT INTO `address_book` (`sid`, `name`, `mobile`, `email`, `address`, `birthday`) VALUES
(7, '李大華2', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(8, '李大華3', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(9, '李大華4', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(10, '李大華5', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(11, '李大華3', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(12, '李大華2', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(13, '李大華3', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(14, '李大華4', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(15, '李大華5', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10'),
(16, '李大華3', '0918111666', 'DRGTSD@DGD.com', '台北市', '1980-10-10');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`sid`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `address_book`
--
ALTER TABLE `address_book`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
