<?php
require __DIR__ . '/__connect_db.php';
$pname = 'data_list';
$title = "543 ";

$t_rs = $mysqli->query("SELECT COUNT(1) FROM `address_book`");
$t_row = $t_rs->fetch_row();
$num_rows = $t_row[0];

//$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
$per_page = 5;
$num_pages = ceil($num_rows/$per_page);

$sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);

$rs = $mysqli->query($sql);

//$row = $rs->fetch_assoc();

?>
<?php include __DIR__. '/__page_head.php' ?>
    <style>
        .del {
            color: red;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>
    <div class="row">
        <div class="col-md-12">
            <nav>
                <ul class="pager">
                    <?php if($page==1): ?>
                        <li class="disabled"><a>First</a></li>
                        <li class="disabled"><a>Previous</a></li>
                    <?php else: ?>
                        <li><a href="?page=1">First</a></li>
                        <li><a href="?page=<?= $page-1 ?>">Previous</a></li>
                    <?php endif ?>
                    <li><?= $page ?> / <?= $num_pages ?></li>
                    <?php if($page==$num_pages): ?>
                        <li class="disabled"><a>Next</a></li>
                        <li class="disabled"><a>Last</a></li>
                    <?php else: ?>
                        <li><a href="?page=<?= $page+1 ?>">Next</a></li>
                        <li><a href="?page=<?= $num_pages ?>">Last</a></li>
                    <?php endif ?>
                </ul>
            </nav>
        </div>

        <div class="col-md-12">



            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>mobile</th>
                    <th>email</th>
                    <th>address</th>
                    <th>birthday</th>
                    <th>edit</th>
                    <th>delete</th>
                </tr>
                </thead>
                <tbody>
                <?php while( $row = $rs->fetch_assoc() ): ?>
                <tr>
                    <th><?= $row['sid'] ?></th>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['mobile'] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td><?= htmlentities($row['address']) ?></td>
                    <td><?= $row['birthday'] ?></td>
                    <td>
                        <a href="data_edit.php?sid=<?= $row['sid'] ?>">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td>
                        <a href="javascript:delete_it(<?= $row['sid'] ?>)">
                            <span class="glyphicon glyphicon-remove del"></span>
                        </a>
                    </td>
                </tr>
                <?php endwhile ?>
                </tbody>
            </table>

        </div>

        <div class="col-md-12">
            <nav>
                <ul class="pagination">
                    <?php for($i=1; $i<=$num_pages; $i++): ?>
                        <li class="<?= $page==$i ? 'active' : '' ?>"><a href="?page=<?=$i?>"><?=$i?></a></li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>

</div>
    <script>

        function delete_it(sid) {
            if(confirm("您確定要刪除編號為 " +sid+ " 的資料嗎?")){
                location.href = "delete_by_sid.php?sid=" + sid;
            }
        }




    </script>
<?php include __DIR__. '/__page_foot.php' ?>