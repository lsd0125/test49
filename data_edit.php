<?php
require __DIR__ . '/__connect_db.php';
$pname = 'data_edit';


$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if( isset($_POST['name']) ){

    $sql = "UPDATE `address_book` SET `name`=?,`mobile`=?,`email`=?,`address`=?,`birthday`=? WHERE `sid`=?";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param("sssssi",
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['address'],
        $_POST['birthday'],
        $sid
        );

    $stmt->execute();

    $affected_rows = $stmt->affected_rows;

    //exit;
}




$sql = "SELECT * FROM `address_book` WHERE `sid` = $sid";
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();


?>
<?php include __DIR__ . '/__page_head.php' ?>
    <style>
        .red {
            color: red;
        }
    </style>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>


        <?php if(isset($affected_rows)): ?>
            <?php if($affected_rows): ?>
                <div class="col-md-12" id="myinfo">
                    <div class="alert alert-success" role="alert">
                        資料修改完成
                    </div>
                </div>

            <?php else: ?>
                    <div class="col-md-12" id="myinfo">
                        <div class="alert alert-warning" role="alert">
                            資料未修改
                        </div>
                    </div>
            <?php endif ?>
            <script>
                setTimeout(function(){
                    $('#myinfo').slideUp();
                }, 3000);
            </script>
        <?php endif ?>

        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">編輯資料</h3></div>
                    <div class="panel-body">

                        <form method="post" onsubmit="return checkForm();">
                            <div class="form-group">
                                <label for="name">姓名</label>  <span class="red">請填寫姓名</span>
                                <input type="text" class="form-control" id="name" name="name" value="<?= $row['name'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="mobile">手機</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" value="<?= $row['mobile'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="email">電郵</label>
                                <input type="text" class="form-control" id="email" name="email" value="<?= $row['email'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="address">地址</label>
                                <textarea  class="form-control" id="address" name="address" cols="30" rows="5"><?= $row['address'] ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="birthday">生日</label>
                                <input type="text" class="form-control" id="birthday" name="birthday" value="<?= $row['birthday'] ?>">
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">修改</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <script>
        function checkForm(){

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if($('#name').val().length < 3 ){
                alert('請填寫正確的姓名');
                return false;
            }

            if($('#mobile').val().length < 3 ){
                alert('請填寫手機號碼');
                return false;
            }

            var email = $('#email').val();

            if(! pattern.test(email)){
                alert('請填寫正確的 email');
                return false;
            }


            return true;
        }




    </script>
<?php include __DIR__ . '/__page_foot.php' ?>